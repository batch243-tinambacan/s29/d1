// [Section] Comparison Query Operators

	/*	$gt / $gte operator
		
		- it allows us to find document that have field number value greater than or equal to a specified value.

		-Syntax:
			db.collectionName.find({field: {$gt : value }});
			db.collectionName.find({field: {$gte : value }});
	*/

	db.users.find({age : {$gt:50}}).pretty(); //$gt

	db.users.find({age : {$gte:21}}).pretty(); //$gte



	/*	$lt / $lte operator
		
		- it allows us to find document that have field number value less than or equal to a specified value.

		-Syntax:
			db.collectionName.find({field: {$lt : value }});
			db.collectionName.find({field: {$gte : value }});
	*/
	db.users.find({age : {$lt:50}}); //$lt
	db.users.find({age : {$lt:76}}); //$lte


	/* $ne operator

		-allows us to find docus that have field number values not equal to a scpecified value
	
		-Syntax:
			db.collectionName.find({field: {$ne : value }});
	*/
	db.users.find({age : {$ne:68}});


	/* $in operator

		-allows us to find docs with specific match one criteria field using different values.
	
		-Syntax:
			db.collectionName.find({field: {$in : [valueA, valueB] }});
	*/

	db.users.find({lastName: {$in : ["Doe", "Hawking"]}})

	db.users.find({courses: {$in : ["React", "HTML"]}})


// [Section] Logical Query Operators

	/*
		$or operator 
		 	- allows us to find documents that match a single criteria from multiple provided search criteria.

			-Syntax:
				db.collectionName.find({$or : [{fieldA: valueA}, {filedB : valueB}]);
		*/
		db.users.find({$or : [{firstName: "Neil"}, {age: 25}]})

		db.users.find({$or : [{firstName: "Neil"}, {age: {$gt : 30}}]})


	/* $and operator
			- allows us to find docs matching multiple provided criteria

			- syntax:
				db.collectionName.find($and : [{fieldA:valueA}, [{fieldB:valueB}])
	*/

		db.users.find({$and : [{firstName: "Neil"}, {age: {$gt : 30}}]})

		db.users.find({$and : [{firstName: "Neil"}, {age: 25}]})
		db.users.find({$and : [{firstName: "Neil"}, {age: {$gt : 25}}]})

// [Section] Field projection

	/* Inclusion
			- it allows us to include specific fields only when retrieving (find method) documents.
			- Syntax: db.users.find({criteria}, {fields: 1})
	*/

		db.users.find({firstName: "Jane"}, 
		{
			firstName: true, //can be 1 for true and 0 forfalse value
			lastName: true, // if string, it will replace the data value in the displayed table but will not replace its value from the db
			age: true
		})

		// another example:
		db.users.find({$and : [{firstName: "Neil"}, {age: {$gt : 30}}]},
		{
			firstName: true,
			lastName: true,
			contact: true,
			age: true
		})

	/* Exclusion
			- allows us to exclude specific fields only when retrieving(find method) documents
	*/

		db.users.find({firstName: "Jane"}, {
			contact: 0,
			department: 0
		})

		db.users.find({$and: [{firstName: "Neil"}, {age: {$gte: 25}}]},
		{
				age: false,
				courses: 0
		})


		db.users.find({firstName: "Jane"}, {
			"contact.phone":1 // exclude other fields and contacts except phone
		})

		db.users.find({firstName: "Jane"}, {
			"contact.phone":0 // exclude phone only, all fields and contacts will be included
		})

// [Section] Evaluation Query Operators
	/*
		- case sensitive query; it allows us to find document that match a specific string pattern using regular expressions.

		- syntax:
			db.users.find({field: $regex : 'pattern', $option: : '$optionValue'})
	*/
		db.users.find({firstName : {$regex : 'N', $options : '$i'}}) // result is all collection with "N" in their firstName



// CRUD Operations

	db.users.updateOne({age: {$lte: 17}},{
		$set: {
			firstName: "Chris",
			lastName: "Mortel"
		}

	})

	db.users.deleteOne({$and: [{firstName: "Chris"}, {lastName: "Mortel"}]})

	db.users.find({$and: [{age: {$lte: 82}}, {age: {$gte : 76}}]})